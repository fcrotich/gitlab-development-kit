#!/usr/bin/env ruby

# frozen_string_literal: true

require 'uri'
require 'net/http'

require_relative '../lib/gdk'

$stdout.sync = true

FileNotFoundError = Class.new(StandardError)

GITLAB_PROJECT_BASE_URL = 'https://gitlab.com/gitlab-org/gitlab'
TOOL_VERSIONS_FILES = [
  { refs: -> { %w[master] }, base_url: GITLAB_PROJECT_BASE_URL },
  { refs: -> { %w[master] }, base_url: GITLAB_PROJECT_BASE_URL, path: 'workhorse' },
  { refs: -> { ['main', "v#{ref_from_remote_file('GITLAB_SHELL_VERSION')}"] }, base_url: 'https://gitlab.com/gitlab-org/gitlab-shell' },
  { refs: -> { ['master', "v#{ref_from_remote_file('GITLAB_PAGES_VERSION')}"] }, base_url: 'https://gitlab.com/gitlab-org/gitlab-pages' },
  { refs: -> { ['master', ref_from_remote_file('GITALY_SERVER_VERSION')] }, base_url: 'https://gitlab.com/gitlab-org/gitaly' },
  { refs: -> { ['main', "v#{ref_from_remote_file('GITLAB_ELASTICSEARCH_INDEXER_VERSION')}"] }, base_url: 'https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer' },
  { refs: -> { %w[main] }, base_url: 'https://gitlab.com/gitlab-org/gitlab-runner' },
  { refs: -> { %w[main] }, base_url: 'https://gitlab.com/gitlab-org/gitlab-ui' },
  { refs: -> { %w[main] }, base_url: 'https://gitlab.com/gitlab-org/gitlab-docs' }
].freeze

HEADER_CONTENT = <<~CONTENT
  # support/asdf-combine generates this file from .tool-versions-gdk and the .tool-versions
  # files from GDK sub-projects.
  #
  # Do not modify this file directly.
  #
CONTENT

def gdk_root
  @gdk_root ||= GDK::Config.new.gdk_root
end

def http_get(url)
  uri = URI.parse(url)
  response = Net::HTTP.get_response(uri)
  raise FileNotFoundError, "Unable to get '#{url}'" unless response.class == Net::HTTPOK

  response.body
end

def read_tool_versions_from(origin, content)
  content.lines.each_with_object({}) do |entry, object|
    next unless (match = entry.match(/^(?<software>\w+) (?<versions>.+)$/))

    object[match[:software]] = match[:versions].split.each_with_object({}) { |e, all| all[e] = [origin] }
  end
end

def write_tool_versions_file(data)
  tool_versions_file.write("#{HEADER_CONTENT}#{data}\n")
end

def ref_from_remote_file(file)
  url = "#{GITLAB_PROJECT_BASE_URL}/-/raw/master/#{file}"
  http_get(url).chomp
end

def tool_versions_file
  @tool_versions_file ||= gdk_root.join('.tool-versions')
end

def tool_versions_gdk_file
  @tool_versions_gdk_file ||= gdk_root.join('.tool-versions-gdk')
end

def format_software_and_versions_data(data)
  output = []

  data.each do |software, version_and_origins|
    output << "# #{software}:"
    version_and_origins.each do |version, origins|
      output << "#   #{version}:"
      origins.each do |origin|
        output << "#     - #{origin}"
      end
    end
    output << '#'
    output << "#{software} #{version_and_origins.keys.join(' ')}"
    output << ''
  end

  output.join("\n")
end

def processs_tool_versions_files(tool_versions_data, quiet: false)
  TOOL_VERSIONS_FILES.each do |entry|
    entry[:refs].call.each do |ref|
      path = entry[:path] ? "#{entry[:path]}/" : ''
      path_to_tool_versions = "#{path}.tool-versions"
      url = "#{entry[:base_url]}/-/raw/#{ref}/#{path_to_tool_versions}"

      $stderr.print '.' unless quiet

      begin
        tool_versions_contents = http_get(url)
      rescue FileNotFoundError
        warn("ERROR: #{url} does not exist.")
        next
      end

      tool_versions_data_for_entry = read_tool_versions_from(url, tool_versions_contents)

      tool_versions_data_for_entry.each do |software, versions|
        tool_versions_data[software] ||= {}
        versions.each do |version, origin|
          tool_versions_data[software][version] ||= []
          tool_versions_data[software][version] |= origin
        end
      end
    end
  end

  tool_versions_data
end

def main(quiet: false)
  tool_versions_data_from_gdk_file = read_tool_versions_from('.tool-versions-gdk', tool_versions_gdk_file.read)

  tool_versions_data = processs_tool_versions_files(tool_versions_data_from_gdk_file.clone, quiet: quiet).sort.each_with_object({}) do |(software, versions), all|
    # We only sort by newest version first *if* not defined in .tool-versions-gdk
    sorted_versions = if tool_versions_data_from_gdk_file[software]
                        versions
                      else
                        versions.sort { |(x_version, _), (y_version, _)| Gem::Version.new(x_version) <=> Gem::Version.new(y_version) }.reverse
                      end

    all[software] = sorted_versions.to_h
  end

  formatted_data = format_software_and_versions_data(tool_versions_data)

  unless quiet
    warn("\n") unless quiet # adds a newline to stderr after the ... activity dots.
    warn("Writing the following to #{tool_versions_file}")
    warn '=' * 80
    warn

    puts formatted_data
  end

  write_tool_versions_file(formatted_data)
end

# ------------------------------------------------------------------------------

main(quiet: ARGV.include?('--quiet'))
